import urllib.request
import bs4 as bs
import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import fbchat

# sauce = urllib.request.urlopen('http://data.nowgoal.com/3in1odds/24_1629398.html').read()
zpravy = []

def hlavni(adresa, driver, cislo, cislo_2, historie_zprav, group_Skupina, client):
    """
    options = webdriver.ChromeOptions()
    options.set_headless(headless=False)
    driver = webdriver.Chrome(chrome_options=options, executable_path=r'C:/chromedriver.exe')"""

    # Stáhne data k celému zápasu
    driver.get("http://data.nowgoal.com/3in1odds/24_" + adresa + ".html")
    WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "div_d")))
    source = driver.page_source
    soup = bs.BeautifulSoup(source, "lxml")

    # Stáhne data k poločasům
    driver.get("http://data.nowgoal.com/3in1odds/24_" + adresa + "_2.html")
    WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "div_d")))
    source_2 = driver.page_source
    soup_2 = bs.BeautifulSoup(source_2, "lxml")

    pocet = 0
    pocet2 = 7
    pocet3 = 11
    tabulka = []
    tabulka_live = []
    spravne = ""
    nazvy = []

    poc = 0
    for h4 in soup.find_all("h4"):
        if poc == 1:
            for nazev1 in h4.find_all("font"):
                nazvy.append(nazev1.text)
        poc += 1

    zapas = str(nazvy[0]) + " vs " + str(nazvy[1])

    for every in soup.find_all("div", {"id": "div_d"}):
        for td in every.find_all("td"):
            if pocet < 11:
                pass
            else:
                if pocet3 % 7 == 0:
                    get = td.get("class")
                    tabulka_live.append(get[0])
                else:
                    pass
                pocet3 += 1
            pocet += 1

    pocet = 0
    pocitadlo = 0
    for every in soup.find_all("div", {"id": "div_d"}):
        for td in every.find_all("td"):
            if pocet < 10:
                pass
            else:
                if pocet2 % 7 == 0:
                    if tabulka_live[pocitadlo] == "hg_blue":
                        get = td.get("style")
                        tabulka.append(get)
                    pocitadlo += 1
                else:
                    pass
                pocet2 += 1

            pocet += 1

    tabulka_n = []

    for kazdy in tabulka:
        novy = kazdy.split(":")
        tabulka_n.append(novy[1])

    celkem_1 = 0
    zelena = 0

    for prvek in tabulka_n:

        if prvek == "red;":
            zelena = 0

        if prvek == "green;":
            zelena += 1

        if zelena == cislo:
            celkem_1 += 1
            zelena = 0

    """Under/Over Half time"""
    b = []
    zdroj = soup_2.find("div", {"id": "div_d"})
    for policko in zdroj.find_all("tr"):
        a = []
        for podpolicko in policko.find_all("td"):
            barva = podpolicko.get("style")
            running = podpolicko.get("class")
            a.append(barva)
            a.append(running)
        if len(a) == 14:
            if a[13] == ["hg_blue"]:
                b.append(a[4])
    celkem_2 = 0
    zelena = 0

    for prvek in b:

        if prvek == "color:red;":
            zelena = 0

        if prvek == "color:green;":
            zelena += 1

        if zelena == cislo:
            celkem_2 += 1
            zelena = 0

    """Handicap Full time"""
    zdroj_3 = soup.find("div", {"id": "div_l"})
    dict = {}
    posun = 0
    for policko_3 in zdroj_3.find_all("tr"):
        a = []
        cisla = []
        for podpolicko in policko_3.find_all("td"):
            barva = podpolicko.get("style")
            running = podpolicko.get("class")
            docasne_cislo = podpolicko.text
            a.append(barva)
            a.append(running)
            cisla.append(docasne_cislo)
        if len(a) == 14:
            neco = ["hg_blue"]
            if a[13] == neco:
                dict[posun] = [cisla[3], a[4], a[8]]
                posun += 1

    for i in range(len(dict)):
        if dict[i][0] == "Closed":
            del dict[i]

    celkem_3 = 0
    mezizustatek = 0
    for i in dict:
        i += cislo_2
        if i > len(dict):
            break
        else:
            try:
                if dict[i][0] != "0":
                    if "-" in dict[i][0]:
                        if dict[i][2] == 'color:green;':
                            mezizustatek += 1
                            if mezizustatek == cislo_2:
                                celkem_3 += 1
                        else:
                            mezizustatek = 0
                    else:
                        if dict[i][1] == 'color:green;':
                            mezizustatek += 1
                            if mezizustatek == cislo_2:
                                celkem_3 += 1
                        else:
                            mezizustatek = 0
            except KeyError:
                pass

    """Handicap Half time"""

    zdroj_4 = soup_2.find("div", {"id": "div_l"})
    dict = {}
    posun = 0
    for policko_4 in zdroj_4.find_all("tr"):
        a = []
        cisla = []
        for podpolicko in policko_4.find_all("td"):
            barva = podpolicko.get("style")
            running = podpolicko.get("class")
            docasne_cislo = podpolicko.text
            a.append(barva)
            a.append(running)
            cisla.append(docasne_cislo)
        if len(a) == 14:
            neco = ["hg_blue"]
            if a[13] == neco:
                dict[posun] = [cisla[3], a[4], a[8]]
                posun += 1

    for i in range(len(dict)):
        if dict[i][0] == "Closed":
            del dict[i]

    celkem_4 = 0
    mezizustatek = 0
    for i in dict:
        i += cislo_2
        if i > len(dict):
            break
        else:
            try:
                if dict[i][0] != "0":
                    if "-" in dict[i][0]:
                        if dict[i][2] == 'color:green;':
                            mezizustatek += 1
                            if mezizustatek == cislo_2:
                                celkem_4 += 1
                        else:
                            mezizustatek = 0
                    else:
                        if dict[i][1] == 'color:green;':
                            mezizustatek += 1
                            if mezizustatek == cislo_2:
                                celkem_4 += 1
                        else:
                            mezizustatek = 0
            except KeyError:
                pass

    """Vypracuje zprávu"""


    fb_zprava = ""
    if celkem_1 or celkem_2 or celkem_3 or celkem_4 > 0:
        zprava = zapas
        if celkem_1 > 0:
            zprava += " Under/Over Full time " + str(celkem_1) + " x,(" + str(cislo) + "),"
        if celkem_2 > 0:
            zprava += " Under/Over Half time " + str(celkem_2) + " x,(" + str(cislo) + "),"
        if celkem_3 > 0:
            zprava += " Handicap Full time " + str(celkem_3) + " x,(" + str(cislo_2) + "),"
        if celkem_4 > 0:
            zprava += " Handicap Half time " + str(celkem_4) + " x,(" + str(cislo_2) + "),"
        if zprava in historie_zprav:
            pass
        else:
            fb_zprava += zprava
            historie_zprav.append(zprava)
            client.send(fbchat.Message(text=fb_zprava),
                thread_id=group_Skupina.uid, thread_type=fbchat.ThreadType.GROUP)
        print(fb_zprava)
    print("konec")